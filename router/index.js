const express = require('express')
const sha1 = require('sha1')

const Wechat = require('../wechat/wechat')
const { url } = require('../config')
const reply = require('./reply')
const Theaters = require('../model/Theater')
const Trailers = require('../model/Trailers')
const Danmus = require('../model/Danmus')
// 引入bodyParser
express.urlencoded({ extended: true})

const Router = express.Router

const router = new Router()

const wechatApi = new Wechat()

// 页面路由
router.get('/search', async (req, res) => {
  /**
   * 生成js-sdk使用的签名
   *  1. 组合参与签名的四个参数：jsapi_ticket(临时票据)，noncestr(随机字符串)，timestamp(时间戳)，url(当前服务器地址)
   *  2. 将其进行字典排序，以'&'拼接在一起
   *  3. 进行sha1加密，最终生成signature
   */
  // 获取票据
  const { ticket } = await wechatApi.fetchJsapiTicket() 
  // 随机字符串
  const noncestr = Math.random().toString().split('.')[0]
  // 时间戳
  const timestamp = Date.now()
  
  // 1. 组合参与签名的四个参数
  const arr = [
    `jsapi_ticket=${ticket}`,
    `noncestr=${noncestr}`,
    `timestamp=${timestamp}`,
    `url=${url}/search`
  ]

  // 2. 将其进行字典排序，以'&'拼接在一起
  const str = arr.sort().join('&')

  // 3. 进行sha1加密，最终生成signature
  const signature = sha1(str)

  res.render('search', {
    signature,
    noncestr,
    timestamp
  }) // 渲染页面
})

// 详情页面的路由
router.get('/detail/:id', async (req, res) => {
  // 获取id
  let id = req.params.id
  // 判断id之是否存在
  if (id) {
    // 去数据库中查找数据
    const data = await Theaters.findOne({doubanId: id}, {_id: 0, __v: 0})
    console.log(data)

    res.render('detail', { data })
  } else {
    res.send('404')
  }
})

// 预告片页面的路由
router.get('/movie', async (req, res) => {
  
  // 去数据库中查找数据
  const data = await Trailers.find({}, {_id: 0, __v: 0})
  console.log(data)

  res.render('movie', { data })
})

// 加载弹幕的路由
router.get('/v3', async (req, res) => {
  // 获取用户发送的请求参数
  const {id} = req.query
  // 查找响应的电影弹幕信息
  const data = await Danmus.find({doubanId: id})

  let resData = []

  data.forEach(item => {
    resData.push([
      item.time,
      item.type,
      item.color,
      item.author,
      item.text
    ])
  })

  res.send({
    code: 0,
    data: resData
  })
})

// 接收拥护的弹幕
router.post('/v3', async (req, res) => {
  /**
   * 弹幕信息是以流式数据发送过来的
   */
  // 获取请求参数
  const {id, author, time, text, color, type} = await new Promise((resolve, reject) => {
    let body = ''
    req.on('data', data => {
      body += data.toString()
    }).on('end', () => {
      console.log(body)
      body = JSON.parse(body)
      resolve(body)
    })
  })
  // 保存到数据库
  await Danmus.create({
    doubanId: id,
    author,
    time,
    text,
    color,
    type
  })

  // 返回响应
  res.send({
    code: 0,
    data: []
  })
})

// 接收处理所有消息
router.use(reply())

module.exports = router