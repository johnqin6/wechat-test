/**
 * 处理用户发送的消息类型和内容, 决定返回不同的内容给用户
 */
const rp = require('request-promise-native')
const Theaters = require('../model/Theater')
const { url } = require('../config')

module.exports = async (message) => {
  let options = {
    toUserName: message.FromUserName,
    fromUserName: message.ToUserName,
    createTime: Date.now(),
    msgType: 'text'
  }

  let content = '您在说什么，我听不懂'
    // 判断用户发送的消息是否是文本内容
  if (message.MsgType === 'text') { // 文本消息
    // 判断用户发送的消息内容具体是什么
    if (message.Content === '热门电影') { // 全匹配
      // 回复热门电影消息数据
      const data = await Theaters.find({}, {title: 1, summary: 1, image: 1, doubanId: 1,  _id: 0})
      // 将回复内容初始化空数组
      content = []
      options.msgType = 'news';
      // 遍历将数据添加进去
      for (let i = 0; i < data.length; i++) {
        let item = data[i]
        content.push({
          title: item.title,
          description: item.summary,
          picUrl: `http://peicjnx2h.bkt.clouddn.com/${item.posterKey}`,
          url: `${url}/detail/${item.doubanId}`
        })
      }
    } else if (message.Content === '首页') {
      content = '落地成盒'
    } else if (message.Content.match('爱')) { //半匹配
      content = '我爱你'
    } else {
      // 搜索用户的指定信息
      // 定义请求地址
      const url = 'https://api.douban.com/v2/movie/search'

      // 发送请求
      const { subjects } = await rq({ method: 'GET', url, json: true, qs: {
        q: message.content, count: 8
      } })
      // 判断subjects是否有值
      if (subjects && subjects.length) {
         // 将回复内容初始化空数组
        content = []
        options.msgType = 'news';
        // 遍历将数据添加进去
        for (let i = 0; i < subjects.length; i++) {
          let item = data[i]
          content.push({
            title: item.title,
            description: '电影评分为：',
            picUrl: item.images.small,
            url: item.alt
          })
        }
      } else {
        content= '暂时没有相关信息'
      }
    }
  } else if (message.MsgType === 'image') { // 图片消息
    // 用户发送图片消息
    options.msgType = 'image'
    options.mediaId = message.MediaId
    console.log(message.PicUrl)
  } else if (message.MsgType === 'voice') { // 语音消息
    // options.msgType = 'voice'
    // options.mediaId = message.MediaId  
    console.log(message.Recognition) // 语音识别结果
     // 搜索用户的指定信息
    // 定义请求地址
    const url = 'https://api.douban.com/v2/movie/search'

    // 发送请求
    const data = await rq({ method: 'GET', url, json: true, qs: {
      q: message.Recognition, count: 8
    } })
    const subjects = data.subjects
    // 判断subjects是否有值
    if (subjects && subjects.length) {
        // 将回复内容初始化空数组
      content = []
      options.msgType = 'news';
      // 遍历将数据添加进去
      for (let i = 0; i < subjects.length; i++) {
        let item = data[i]
        content.push({
          title: item.title,
          description: '电影评分为：',
          picUrl: item.images.small,
          url: item.alt
        })
      }
    } else {
      content= '暂时没有相关信息'
    }
  } //else if (message.MsgType === 'video') { // 视频消息
  //   options.msgType = 'video'
  //   options.mediaId = message.MediaId 
  //   console.log(message.ThumbMediaId) // 视频消息缩略图的媒体id，可以调用获取临时素材接口拉取数据
  // } else if (message.MsgType === 'shortvideo') { // 小视频消息
  //   options.msgType = 'shortvideo'
  //   options.mediaId = message.MediaId // 语音识别结果
  //   console.log(message.ThumbMediaId) // 视频消息缩略图的媒体id，可以调用获取临时素材接口拉取数据
  // } else if (message.MsgType === 'location') {
  //   content = `纬度：${message.Location_X},经度：${message.Location_Y}, 缩放大小：${message.Scale}, 位置信息：${message.Label}`
  // } 
  else if (message.MsgType === 'event') {
    if (message.Event === 'subscribe') {
      // 用户订阅事件
      content = '欢迎您关注公众号, 请按照一下提示进行操作~ \n' +
            '回复 首页 查看电影预告片页面 \n' +
            '回复 热门 查看最热门的电影 \n' +
            '回复 文本 搜索电影信息 \n' +
            '回复 语音 搜索电影信息 \n' +
            '也可以点击下面的菜单按钮，了解硅谷电影公众号';
      // 判断扫描谁的二维码
      if (message.EventKey) {
        content = '用户是扫描带参数的二维码事件'
      }
    } else if (message.Event === 'unsubscribe') { // 取消订阅
      console.log('无情')
    } else if (message.Event === 'SCAN') {
      content = '用户已经关注过，再扫描带参数的二维码事件'
    } else if (message.Event === 'LOCATION') { // 上报地理位置事件
      content = `纬度：${message.Latitude},经度：${message.Longitude}, 精度：${message.Precision}`
    } else if (message.Event === 'CLICK') { // 点击菜单拉取消息时的事件推送
      content = '请按照一下提示进行操作~ \n' +
            '回复 首页 查看电影预告片页面 \n' +
            '回复 热门 查看最热门的电影 \n' +
            '回复 文本 搜索电影信息 \n' +
            '回复 语音 搜索电影信息 \n' +
            '也可以点击下面的菜单按钮，了解硅谷电影公众号';
    }
  }
  options.content = content

  return options
}