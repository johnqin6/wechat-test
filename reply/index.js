/**
 * 验证服务器的有效性
 * 1. 微信服务器分辨开发者服务器是哪个
 *   - 测试号管理页面上填写开发者服务器地址
 *     - 使用ngrok内网穿透（或国内花生壳），将本地端口开启的服务映射外网跨域访问一个网址
 *     - ngrok http 3000
 *   - 填写token
 *     - 参与微信签名加密的一个参数
 * 2. 开发者服务器 - 验证消息是否来自于微信服务器
 *   目的：计算得出signature微信加密签名，和微信传递过来的signature对比，
 *        一样说明来自微信服务器。
 *   1. 将参与微信加密签名的三个参数(timestamp, nonce,token)安装字典顺序排序并组合形成一个数组
 *   2. 将数组里所有参数拼接成一个字符串，进行sha1加密
 *   3. 加密完成就生成一个signatrue,和微信发送的进行对比
 *     一样就说明来自微信服务器，返回echostr给微信服务器,否则返回error
 */
const sha1 = require('sha1')
const config = require('../config')
const { getUserDataAsync, parseXMLAsync, formatMessage } = require('../utils/tool') // 引入tool模块
const template = require('./template')
const reply = require('./reply')

// 这样写好处，可传参
module.exports = () => {
  return async (req, res, next) => {
    console.log(req.query) // 微信服务器提交的参数
    /**
     * { signature: '266349d29feadfa1d9a914e9db6e2860eb633ba0', // 微信的加密签名
         echostr: '3286686535251247103', // 微信的随机字符串
         timestamp: '1569031743', // 发送请求的时间戳
         nonce: '393482375' }  // 微信的随机数字
     */
    const {signature, echostr, timestamp, nonce} = req.query
    const {token} = config
    
    /*
      // 1. 将参与微信加密签名的三个参数(timestamp, nonce,token)安装字典顺序排序并组合形成一个数组
      const arr = [timestamp, nonce, token]
      const arrSort = arr.sort()
      console.log(arrSort)
      // 2. 将数组里所有参数拼接成一个字符串，进行sha1加密
      const str = arr.join('')
      console.log(str)
      const sha1Str = sha1(str)
      console.log(sha1Str)
    */

    // 简写
    const sha1Str = sha1([timestamp, nonce, token].sort().join(''))

    /**
     * 微信服务器会发送两种类型的消息给开发者服务器
     *   1. GET请求
     *     - 验证服务器的有效性
     *   2. POST请求
     *     - 微信服务器会将用户发送的数据以POST请求的方式转发到开发者服务器上
     */
    if (req.method === 'GET') {
      //  加密完成就生成一个signatrue,和微信发送的进行对比
      if (sha1Str === signature) {
        // 一样就说明来自微信服务器，返回echostr给微信服务器
        res.send(echostr)
      } else {
        res.send('error')
      }
    } else if (req.method === 'POST') {
      // 微信服务器会将用户发送的数据以post请求的方式转发到开发者服务器
      // 验证消息来自与微信服务器
      if (sha1Str !== signature) { // 不一样，说明不是来自微信服务器
        res.send('error')
      }
      
      // 接收请求体中的数据，流式数据
      const xmlData = await getUserDataAsync(req)
      // console.log(xmlData)
      /**
       * <xml><ToUserName><![CDATA[gh_1997489f21a7]]></ToUserName> // 开发者id
        <FromUserName><![CDATA[orq9Xwv51Kxal00Da9hlYKJWyyfA]]></FromUserName> // 用户openid
        <CreateTime>1569051229</CreateTime> // 发送的时间戳
        <MsgType><![CDATA[text]]></MsgType> // 消息类型
        <Content><![CDATA[好好]]></Content> // 消息内容
        <MsgId>22463412550033231</MsgId>   // 消息id, 微信服务器默认保存3天用户发送的数据，通过此id三天内可找到此消息
        </xml>
       */
      // 将xml数据解析为js对象
      const jsData = await parseXMLAsync(xmlData)
      // console.log(jsData)
      /**
       * { xml:
          { ToUserName: [ 'gh_1997489f21a7' ],
            FromUserName: [ 'orq9Xwv51Kxal00Da9hlYKJWyyfA' ],
            CreateTime: [ '1569051955' ],
            MsgType: [ 'text' ],
            Content: [ '22158565' ],
            MsgId: [ '22463425181735171' ] } 
          }
       */

       // 格式化数据
       const message = formatMessage(jsData)
       console.log(message)
       /**
        * { ToUserName: 'gh_1997489f21a7',
            FromUserName: 'orq9Xwv51Kxal00Da9hlYKJWyyfA',
            CreateTime: '1569053741',
            MsgType: 'text',
            Content: '123456',
            MsgId: '22463450875848025' }
        */

       // 简单的自动回复，回复文本内容
       /**
        * 一旦遇到以下情况，微信都会在公众号会话中，向用户下发系统提示“该公众号暂时无法提供服务，请稍后再试”：
          1、开发者在5秒内未回复任何内容 
          2、开发者回复了异常数据，比如JSON数据,xml数据有多余的空格等
        */
      // 将reply函数改成了async函数，此时需要await才能拿到返回的值
      const options = await reply(message)

      // 回复用户的消息
      let replyMessage = template(options)
      // 返回响应给微信服务器
      res.send(replyMessage)

      // console.log(req.query)
      /**
       * { signature: '5c822f5fee6a56e8fee8961d178b74cbeb418223',
          timestamp: '1569050015',
          nonce: '720487330',
          openid: 'orq9Xwv51Kxal00Da9hlYKJWyyfA' // 用户的openid
        }
       */
      // 如果开发者服务器没有返回响应给微信服务器，微信服务器会发送三次过来
      // res.send('') // 为测试时防止微信服务器会发送三次过来
    } else {
      res.send('error')
    }
  }
}
