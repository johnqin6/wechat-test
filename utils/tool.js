/**
 * 工具函数包
 */
const { writeFile, readFile } = require('fs')
const { resolve } = require('path')
const { parseString } = require('xml2js') // 专门将xml转为js对象的依赖
module.exports = {
  // 异步获取用户数据
  getUserDataAsync (req) {
    return new Promise((resolve, reject) => {
      let xmlData = ''
      req.on('data', data => {
        //当流式数据传递过来时，会触发当前事件，将数据注入到回调函数中
        xmlData += data.toString()
      }).on('end', () => {
        // 当数据接收完毕时，会触发
        resolve(xmlData)
      })
    })
  },
  // 将xml数据转为js数据
  parseXMLAsync (xmlData) {
    return new Promise((resolve, reject) => {
      parseString(xmlData, {trim: true}, (err, data) => {
        if (!err) {
          resolve(data)
        } else {
          reject('parseXMLAsync方法出了问题：' + err)
        }
      })
    })
  },
  // 格式化已转为js数据的xml数据
  formatMessage(jsData) {
    let message = {}
    // 获取xml对象
    jsData = jsData.xml
    // 判断是否是一个对象
    if (typeof jsData === 'object') {
      // 遍历对象
      for (let key in jsData) {
        // 获取属性值
        let value = jsData[key]
        // 过滤空的数据
        if ((Array.isArray(value) && value > 0) || value ) {
          message[key] = value[0] // 将合法的数据复制到message
        }
      }
    }
    return message
  },
  // 将数据写入文件
  writeFileAsync (data, fileName) {
    // 将对象转为字符串
    data = JSON.stringify(data)
    const filePath = resolve(__dirname, fileName) //绝对路径
    return new Promise((resolve, reject) => {
      writeFile(filePath, data, err => {
        if (!err) {
          console.log('文件保存成功！')
          resolve()
        } else {
          reject('writeFileAsync方法出错：' + err)
        }
      })
    })
  },
  // 读取文件的数据
  readFileAsync (fileName) {
    const filePath = resolve(__dirname, fileName) //绝对路径
    return new Promise((resolve, reject) => {
      readFile(filePath, (err, data) => {
        if (!err) {
          console.log('文件读取成功！')
          data = JSON.parse(data)
          resolve(data)
        } else {
          reject('readFileAsync方法出错：' + err)
        }
      })
    })
  }
}
