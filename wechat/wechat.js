/**
 * 获取access_token
 *   是什么？是全局唯一接口调用凭证
 * 特点：
 *   - 唯一的
 *   - 有效期为2小时，提前5分钟请求
 *   - 接口权限 每天2000次
 * 请求地址：
 *   https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
 * 请求方式：
 *   GET
 * 
 * 设计思路：
 *   1. 首次本地没有，发送请求获取access_token, 保存下来（本地文件）
 *   2. 第二次及以后
 *      - 先本地读取文件，判断是否过期
 *        - 过期： 重新请求获取access_token, 保存下拉覆盖之前文件（保证文件是唯一的） 
 *        - 未过期：直接使用
 * 思路整理：
 *   读取本地文件(readAccessToken)
 *     - 本地有文件
 *       - 判断是否过期(isValidAccessToken)
 *         - 过期：重新请求获取access_token(getAccessToken), 保存下拉覆盖之前文件（保证文件是唯一的）(saveAcessToken)
 *         - 未过期：直接使用
 *     - 本地没有文件
 *       - 发送请求获取access_token, 保存下来（本地文件），直接使用。
 */
// 只需要引入request-promise-native依赖 request依赖是该依赖需要的
const rp = require('request-promise-native') // 用于服务端请求（promise形式）
const request = require('request')
const {appID, appsecret} = require('../config')
const menu = require('./menu')
const api = require('../utils/api')
const { writeFileAsync, readFileAsync } = require('../utils/tool')
const { createReadStream, createWriteStream } = require('fs')
const path = require('path')

class Wechat {
  constructor () {

  }

  /**
   * 获取access_token
   */
  getAccessToken () {
    const url = `${api.accessToken}&appid=${appID}&secret=${appsecret}`
    // 发送请求 返回promise是为了将请求结果返回出去
    return new Promise((resolve, reject) => {
      rp({
        method: 'GET',
        url,
        json: true // 将数据转为json
      }).then(res => {
        console.log(res)
        /**
         * { access_token:
              '25_DoJkeo7h0vzelTJH_-sgKqddyp0huo6iq-Ur4qdRMYwqTOQm0aBySJ6Fa6mu7YhKPaN2ruGNSpp73RSQbSXA2lPfiyoI8l1JmsPVF7ecEzuEv3qlG-ZaoYa6FLoELVhAGAWDN',
             expires_in: 7200 }
         */
        // 设置access_token的过期时间 300 = 5 * 60
        res.expires_in = Date.now() + (res.expires_in - 300) * 1000
        resolve(res)
      }).catch(err => {
        console.log(err)
        reject('getAccessToken出错：' + err)
      })
    })
  }

  /**
   * 保存access_token
   * @param accessToken 要保存的凭证
   */
  saveAccessToken(accessToken) {
    return writeFileAsync(accessToken, 'accessToken.txt')
  }

  /**
   * 读取本地文件的access_token
   */
  readAccessToken() {
    return readFileAsync('accessToken.txt')
  }

  /**
   * 检测accessToken是否是有效的
   * @param {*} data
   */
  isValidAccessToken(data) {
    // 检测传入的参数是否是有效的
    if (!data && !data.access_token && !data.expires_in) {
      // 代表access_token无效的
      return false
    }

    // 检测access_token是否在有效期内
    /** 
     *  if (data.expires_in < Date.now()) {
     *   // 过期了
     *  return false
     *  } else {
     *   return true
     *  }
     */
    return data.expires_in > Date.now()
  }
  /**
   * 获取没有过期的access_token
   * return {Promise<any>} access_token
   */
  fetchAccessToken () {
    if (this.access_token && this.expires_in && this.isValidAccessToken(this)) {
      // 说明之前保存过access_token, 并且它是有效的，直接使用
      return Promise.resolve({
        access_token: this.access_token,
        expires_in: this.expires_in
      })
    }
    // fetchAccessToken函数的返回值
    return this.readAccessToken().then(async res => {
        // 本地有文件
        // 判断是否过期
        if (this.isValidAccessToken(res)) {
          // 有效的
          // resolve(res) 
          return Promise.resolve(res) //为方便继续.then
        } else { // 过期了
          // 发送请求获取access_token
          const res = await this.getAccessToken()
          // 保存下来，直接使用
          await this.saveAccessToken(res)
          // 将请求回来的access_token返回出去
          // resolve(res)
          return Promise.resolve(res)
        }
      }).catch(async err => {
        // 本地没有文件
        // 发送请求获取access_token
        const res = await this.getAccessToken()
        // 保存下来，直接使用
        await this.saveAccessToken(res)
        // 将请求回来的access_token返回出去
        // resolve(res)
        return Promise.resolve(res)
      }).then(res => {
        // 将access_token挂载到this上
        this.access_token = res.access_token
        this.expires_in = res.expires_in
        // 返回res包装了一层promise对象(此对象成功的状态)
        // this.readAccessToken（）最终的返回值
        return Promise.resolve(res)
      })
  }

  /**
   * 获取jsapi_ticket
   */
  getJsapiTicket () {
    // 发送请求 返回promise是为了将请求结果返回出去
    return new Promise(async (resolve, reject) => {
      // 获取access_token
      const data = await this.fetchAccessToken()
      const url = `${api.ticket}&access_token=${data.access_token}`
      rp({
        method: 'GET',
        url,
        json: true // 将数据转为json
      }).then(res => {
        console.log(res)
        resolve({
          ticket: res.ticket,
          expires_in: Date.now() + (res.expires_in - 300) * 1000
        })
      }).catch(err => {
        reject('getJsapiTicket出错：' + err)
      })
    })
  }

  /**
   * 保存jsapi_ticket
   * @param jsapiTicket 要保存的凭证
   */
  saveJsapiTicket(jsapiTicket) {
    return writeFileAsync(jsapiTicket, 'jsapiTicket.txt')
  }

  /**
   * 读取本地文件的jsapi_ticket
   */
  readJsapiTicket() {
    return readFileAsync('jsapiTicket.txt')
  }

  /**
   * 检测jsapi_ticket是否是有效的
   * @param {*} data
   */
  isValidJsapiTicket(data) {
    // 检测传入的参数是否是有效的
    if (!data && !data.ticket && !data.expires_in) {
      // 代表ticket无效的
      return false
    }
    return data.expires_in > Date.now()
  }
  /**
   * 获取没有过期的jsapi_ticket
   * return {Promise<any>} jsapi_ticket
   */
  fetchJsapiTicket () {
    if (this.ticket && this.ticket_expires_in && this.isValidJsapiTicket(this)) {
      // 说明之前保存过ticket, 并且它是有效的，直接使用
      return Promise.resolve({
        ticket: this.ticket,
        expires_in: this.expires_in
      })
    }
    // fetchAccessToken函数的返回值
    return this.readJsapiTicket().then(async res => {
        // 本地有文件
        // 判断是否过期
        if (this.isValidJsapiTicket(res)) {
          // 有效的
          // resolve(res) 
          return Promise.resolve(res) //为方便继续.then
        } else { // 过期了
          // 发送请求获取ticket
          const res = await this.getJsapiTicket()
          // 保存下来，直接使用
          await this.saveJsapiTicket(res)
          // 将请求回来的ticket返回出去
          // resolve(res)
          return Promise.resolve(res)
        }
      }).catch(async err => {
        // 本地没有文件
        // 发送请求获取ticket
        const res = await this.getJsapiTicket()
        // 保存下来，直接使用
        await this.saveJsapiTicket(res)
        // 将请求回来的ticket返回出去
        // resolve(res)
        return Promise.resolve(res)
      }).then(res => {
        // 将ticket挂载到this上
        this.ticket = res.ticket
        this.ticket_expires_in = res.expires_in
        // 返回res包装了一层promise对象(此对象成功的状态)
        return Promise.resolve(res)
      })
  }
  
  /**
   * 创建菜单
   * @param {*} menu 菜单配置对象
   */
  createMenu (menu) {
    return new Promise(async (resolve, reject) => {
      try {
        //获取access_token
        const data = await this.fetchAccessToken()
        // 定义请求地址
        const url = `${api.menu.create}?access_token=${data.access_token}`
        // 发送请求
        const result = await rp({method: 'POST', url, json: true, body: menu})
        resolve(result)
      } catch (e) {
        reject('createMenu出错：' + e)
      }
    })
  }

  /**
   * 删除菜单
   */
  deleteMenu () {
    return new Promise(async (resolve, reject) => {
      try {
        const data = await this.fetchAccessToken()
        const url = `${api.menu.delete}?access_token=${data.access_token}`
        console.log(url)
        const result = await rp({method: 'GET', url, json: true})
        resolve(result)
      } catch (e) {
        reject('deleteMenu出错：' + e)
      }
    })
  }

  /**
   * 上传临时素材
   * @param type 媒体类型
   * @param fileName 媒体文件地址
   */
  uploadTemporaryMaterial (type, fileName) {
    const filePath = path.resolve(__dirname, '../media', fileName)
    return new Promise(async (resolve, reject) => {
      try { // 可能出错的代码
        // 获取access_token
        const data = await this.fetchAccessToken()
        // 定义请求地址
        const url = `${api.temporary.load}access_token=${data.access_token}&type=${type}`
        
        // 以流的形式读取
        const formData = {
          media: createReadStream(filePath)
        }
        // 以form表单的形式发送请求
        const result = await rp({ method: 'POST', url, json: true, formData })
        // 将数据返回给用户
        resolve(result)
      } catch(e) {
        reject('uploadTemporaryMaterial出错' + e)
      }
    })
  }
  
  /**
   * 获取临时素材
   * @param {*} type 
   * @param {*} mediaId 
   * @param {*} fileName 
   */
  getTemporaryMaterial(type, mediaId, fileName) {
    const filePath = path.resolve(__dirname, '../media', fileName)
    return new Promise(async (resolve, reject) => {
      // 获取access_token
      const data = await this.fetchAccessToken()
      // 定义请求地址
      let url = `${api.temporary.get}access_token=${data.access_token}&media_id=${mediaId}`
      // 判断是否是视频文件
      if (type === 'video') {
        // 视频文件只支持http协议
        url = url.replace('https://', 'http://')
        // 发送请求
        const data = await rp({method: 'GET', url, json: true})
        // 返回出去
        resolve(data)
      } else {
        // 其他类型的文件
        request(url)
          .pipe(createWriteStream(filePath))
          .on('close', resolve) // 当文件读取完毕时，可读流会自动关闭，一旦关闭触发close事件，从而调用resolve外部文件读取完毕了
      }
    })
  }

  /**
   * 上传永久素材
   * @param {*} type 
   * @param {*} material 素材
   * @param {*} body 
   */
  uploadPermanmentMaterial (type, material, body) {
    return new Promise(async (resolve, reject) => {
      try {
        // 获取access_token
        const data = await this.fetchAccessToken()
        // 请求的配置对象
        let options = {
          method: 'POST',
          json: true
        }
        // 定义请求地址
        if (type === 'news') {
          // 上传图文消息
          options.url = `${api.permanment.uploadNews}access_token=${data.access_token}`
          options.body = material
        } else if (type === 'pic') {
          // 上传图文消息中的图片
          options.url = `${api.permanment.uploadNews}access_token=${data.access_token}`
          options.formData = {
            media: createReadStream(path.join(__dirname, '../media', material))
          }
        } else {
          // 其他媒体类型的上传
          options.url = `${api.permanment.uploadOthers}access_token=${data.access_token}&type=${type}`
          options.formData = {
            media: createReadStream(path.join(__dirname, '../media', material))
          }
          // 视频素材，需要多提交一个表单
          if (type === 'video') {
            options.body = body
          }
        }

        // 发送请求
        const result = await rp(options)
        // 返回
        resolve(result)
      } catch(err) {
        reject('uploadPermanmentMaterial出错' + err)
      }
    })
  }

  /**
   * 获取永久素材
   * @param {*} type 
   * @param {*} media 
   * @param {*} fileName 
   */
  getPermanmentMaterial (type, media, fileName) {
    return new Promise(async (resolve, reject) => {
      try {
        // 获取access_token
        const data = await this.fetchAccessToken()
        // 定义请求地址
        let url = `${api.temporary.get}access_token=${data.access_token}`
        let options = {method: 'POST', url, json: true, body: { media_id: media }};
        // 发送请求
        if (type === 'news' || 'video') {
          const data = await rp(options)
          resolve(data)
        } else {
          request(options)
            .pipe(createWriteStream(path.join(__dirname, '../media', fileName)))
            .once('close', resolve)
        }
      } catch (err) {
        // reject('getPermanmentMaterial出错' + err) // reject会终止程序
        resolve('getPermanmentMaterial出错' + err) // resolve不会终止程序，之抛出错误信息
      }
    })
  }

  /**
   * 上传素材
   * @param {*} type 
   * @param {*} material 
   * @param {*} body
   * @param {*} isPermanent 是不是永久素材 
   */
  uploadMaterial (type, material, body, isPermanent) {
    return new Promise(async (resolve, reject) => {
      try {
        // 获取access_token
        const data = await this.fetchAccessToken()
        // 请求的配置对象
        let options = {
          method: 'POST',
          json: true,
          formData: {
            media: createReadStream(path.join(__dirname, '../media', material))
          }
        }
        if (isPermanent) { // 永久素材
          // 定义请求地址
          if (type === 'news') {
            // 上传图文消息
            options.url = `${api.permanment.uploadNews}access_token=${data.access_token}`
            options.body = material
            options.formData = null
          } else if (type === 'pic') {
            // 上传图文消息中的图片
            options.url = `${api.permanment.uploadNews}access_token=${data.access_token}`
          } else {
            // 其他媒体类型的上传
            options.url = `${api.permanment.uploadOthers}access_token=${data.access_token}&type=${type}`
            // 视频素材，需要多提交一个表单
            if (type === 'video') {
              options.body = body
            }
          }
        } else {
          // 临时素材
          options.url = `${api.temporary.load}access_token=${data.access_token}&type=${type}`
        }
        
        // 发送请求
        const result = await rp(options)
        // 返回
        resolve(result)
      } catch(err) {
        reject('uploadPermanmentMaterial出错' + err)
      }
    })
  }
}

module.exports = Wechat

// (async () => {
//   // 模拟测试
//   const w = new Wechat()
//   // 删除之前定义的菜单 自定义菜单只有先删除再创建才能生效
//   let result = await w.deleteMenu()
//   console.log(result)
//   // 创建新的菜单
//   result = await w.createMenu(menu)
//   console.log(result)
//   // const data = await w.fetchJsapiTicket()
//   // console.log(data)
// })()

// 模拟测试
// const w = new Wechat()
// w.getAccessToken()
// new Promise((resolve, reject) => {
//   w.readAccessToken().then(res => {
//     // 本地有文件
//     // 判断是否过期
//     if (w.isValidAccessToken(res)) {
//       // 有效的
//       resolve(res)
//     } else { // 过期了
//       // 发送请求获取access_token
//       w.getAccessToken().then(res => {
//         // 保存下来，直接使用
//         w.saveAccessToken(res).then(() => {
//           resolve(res)
//         })
//       })
//     }
//   }).catch(err => {
//     // 本地没有文件
//     // 发送请求获取access_token
//     w.getAccessToken().then(res => {
//       // 保存下来，直接使用
//       w.saveAccessToken(res).then(() => {
//         resolve(res)
//       })
//     })
//   })
// }).then(res => {
//   console.log(res)
// })
