const express = require('express')
const router = require('./router')
const app = express()

// 配置模板资源目录
app.set('views', './views')
// 配置模板引擎
app.set('view engine', 'ejs')

app.use(router)

app.listen(3000, () => {
  console.log('服务器地址：http://localhost:3000')
})
