const Trailers = require('../../model/Trailers')

module.exports = async data => {
  
  for (let i = 0; i < data.length; i++) {
    let item = data[i]

    const result = await Trailers.create({
      title: item.title,
      rating: item.rating,
      runtime: item.runtime,
      directors: item.directors,
      casts: item.casts,
      image: item.image,
      cover: item.cover,
      doubanId: item.doubanId,
      genre: item.genre,
      summary: item.summary,
      releaseDate: item.releaseDate,
      link: item.link
    })
    console.log('数据保存成功')
  }
  
}
