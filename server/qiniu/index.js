/**
 * 将数据库的图片上传到七牛服务器
 */
const upload = require('./upload')
const nanoid = require('nanoid') // 生成唯一key值的轻量级库
module.exports = async (key, Model) => {
  /**
   * 1. 获取数据库中的图片链接
   * 2. 上传七牛
   * 3. 保存key值到数据库中
   */
  // 去数据库中找没有上传图片的文档对象
  const movies = await Model.find({ $or: [
    { [key]: '' },
    { [key]: null },
    { [key]: { $exists: false} },
  ]})

  for (let i = 0; i < movies.length; i++) {
    let movie = movies[i]
    
    // 上传图片到七牛
    let url = movie.image
    let filename = '.jpg'
    if (key === 'coverKey') {
      url = movie.cover
    } else if (key === 'videoKey') {
      url = movie.link
      filename = '.mp4'
    }
    
    let filename = nanoid(10) + filename

    await upload(url, filename)

    // 保存key值到数据库中
    movie[key] = filename
    await movie.save()
  }
}
