// const db = require('../db')
const theatersCrawler = require('./crawler/theatersCrawler')
const saveTheaters = require('./save/saveTheaters')
const uploadToQiniu = require('./qiniu')
const trailersCrawler = require('./crawler/trailersCrawler')
const Theaters = require('../model/Theater')
const Trailers = require('../model/Trailers')

// 连接数据库
// await db;
// 爬取数据
// theatersCrawler().then(res => {
//   // 将数据保存到数据库
//   saveTheaters(res)
// });

// setTimeout(() => {
//   uploadToQiniu('postKey', Theaters)
// }, 3000)

// trailersCrawler()

uploadToQiniu('postKey', Trailers)
uploadToQiniu('coverKey', Trailers)
uploadToQiniu('videoKey', Trailers)

