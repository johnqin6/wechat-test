// 爬取热门电影
// puppeteer 谷歌开发的爬虫库 开发环境使用，线上环境无需使用
const puppeteer = require('puppeteer');

const url = 'https://movie.douban.com/coming'

module.exports = async () => {
  // 1. 打开浏览器
  const browser = await puppeteer.launch({
    args: ['--no-sandbox'],
    headless: true  // 已无头浏览器打开，没有界面显示，在后台显示
  })
  // 2. 创建tab标签页
  const page = await browser.newPage()
  // 3. 跳转到指定网址
  await page.goto(url, {
    waitUntil: 'networkidle2' // 等待网络空闲时，再跳转加载页面
  })
  // 4. 等待网址加载完成，开始抓取数据
  // 开启延时器，延时2秒再开始爬取数据
  await timeout()
  let result = await page.evaluate(() => {
    // 对加载好的页面进行dom操作
    let mList = []
    const $trs = $('.coming_list>tbody>tr') // 所有即将上映电影的li
    for (let i = 0; i < $trs.length; i++) {
      let trDom = $trs[i]
      // 获取想看的人数
      let num = parseInt($(trDom).find('td').last().html())
      let href
      // 获取想看人数大于100的电影
      if (num > 100) {
        href = $(trDom).find('td').eq(1).find('a').attr('href')
      }
      if (href) {
        mList.push(href)
      }
    }
    return mList
  })
  let moviesData = []
  console.log(result)
  // 遍历爬取预告片详情信息
  for (let i = 0; i < result.length; i++) {
    let url = result[i] // 网址
    if (url === null) return
    
    // 跳转到详情页
    await page.goto(url, {
      waitUntil: 'networkidle2' // 等待网络空闲时，再跳转加载页面
    })
    // 爬取其他数据
    let itemResult = await page.evaluate(() => {
      // 类型
      const title = $('[property="v:itemreviewed"]').html()
      const directors = $('[rel="v:directedBy"]').html() // 导演
      const directors = $('[rel="v:directedBy"]').html() // 海报图
      const image = $('[rel="v:image"]').attr('src') // 演员
      let doubanId = $('.a_show_login.link-sharing').attr('share-id')
      for (let j = 0; j < 3; j++) {
        casts.push($('[rel="v:starring"]')[j].innerText)
      }
      let genre = []
      // 类型
      const $genre = $('[property="v:genre"]').html()
      for (let j = 0; j < $genre.length; j++) {
        genre.push($genre[j].innerText)
      }
      // 简介
      const summary = $('[property="v:summary"]').html().replace(/\s+/g, '')
      // 上映日期
      const releaseDate = $('[property="v:initialReleaseDate"]')[0].innerText
      // 片长
      const runtime = $('[property="v:runtime"]')[0].innerText
      // 评分
      const rating = $('[property="v:average"]')[0].innerText
      // 预告片电影网址
      const href = $('.related-pic-video').attr('href')
      // 封面
      const cover = $('.related-pic-video').attr('background-image')
      // 给单个对象添加属性
      return {
        title,
        directors,
        casts,
        releaseDate,
        runtime,
        rating,
        href,
        cover,
        image,
        genre,
        doubanId,
        summary
      }
    })
    // 在最后给当前对象添加属性
    // 在evaluate函数中没办法读取到服务器的变量
    moviesData.push(itemResult)
  }
  console.log(moviesData)

  // 爬取预告片电影链接
  for (let i = 0; i < moviesData.length; i++) {
    let item = moviesData[i] // 网址
    let url = item.href
    
    // 跳转到详情页
    await page.goto(url, {
      waitUntil: 'networkidle2' // 等待网络空闲时，再跳转加载页面
    })
    // 爬取其他数据
    item.link = await page.evaluate(() => {
      // 类型
      const link = $('video>source').attr('src')
     
      // 给单个对象添加属性
      return link
    })
  }
  console.log(moviesData)
  // 5. 关闭浏览器
  await browser.close()
  
  // 最终会将数据全部返回出去
  return moviesData
}

function timeout() {
  return new Promise(resolve => setTimeout(resolve, 2000))
}