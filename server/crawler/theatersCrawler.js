// 爬取热门电影
// puppeteer 谷歌开发的爬虫库 开发环境使用，线上环境无需使用
const puppeteer = require('puppeteer');

const url = 'https://movie.douban.com/cinema/nowplaying/suzhou/'

module.exports = async () => {
  // 1. 打开浏览器
  const browser = await puppeteer.launch({
    args: ['--no-sandbox'],
    headless: true  // 已无头浏览器打开，没有界面显示，在后台显示
  })
  // 2. 创建tab标签页
  const page = await browser.newPage()
  // 3. 跳转到指定网址
  await page.goto(url, {
    waitUntil: 'networkidle2' // 等待网络空闲时，再跳转加载页面
  })
  // 4. 等待网址加载完成，开始抓取数据
  // 开启延时器，延时2秒再开始爬取数据
  await timeout()
  let result = await page.evaluate(() => {
    // 对加载好的页面进行dom操作
    let mList = []
    const $list = $('#nowplaying>.mod-bd>.lists>.list-item') // 所有热门电影的li
    // 只取8条数据
    for (let i = 0; i < 8; i++) {
      let liDom = $list[i]
      let doubanId = $(liDom).attr('id')
      let title = $(liDom).data('title') // 获得自定义属性data-title 标题
      let rating = $(liDom).data('score') // 评分
      let runtime = $(liDom).data('duration') // 电影时长
      let directors = $(liDom).data('director') // 导演
      let casts = $(liDom).data('actors') // 主演
      // 电影海报图
      let image = $(liDom).find('.poster>a>img').attr('src')
      // 电影详情页链接
      let href = $(liDom).find('.poster>a').attr('href')

      mList.push({
        doubanId,
        title,
        rating,
        runtime,
        directors,
        casts,
        image,
        href
      })
    }
    return mList
  })
  console.log(result)
  // 遍历爬取到的8条数据
  for (let i = 0; i < result.length; i++) {
    let item = result[i] // 获取条目信息
    let url = result[i].href // 网址
    
    // 跳转到详情页
    await page.goto(url, {
      waitUntil: 'networkidle2' // 等待网络空闲时，再跳转加载页面
    })
    // 爬取其他数据
    let itemResult = await page.evaluate(() => {
      let genre = []
      // 类型
      const $genre = $('[property="v:genre"]')
      for (let j = 0; j < $genre.length; j++) {
        genre.push($genre[j].innerText)
      }

      // 简介
      const summary = $('[property="v:summary"]').html().replace(/\s+/g, '')
      // 给单个对象添加属性
      return {
        genre,
        summary
      }
    })
    // 在最后给当前对象添加属性
    // 在evaluate函数中没办法读取到服务器的变量
    item.genre = itemResult.genre
    item.summary = itemResult.summary
  }
  console.log(result)
  // 5. 关闭浏览器
  await browser.close()
  
  // 最终会将数据全部返回出去
  return result
}

function timeout() {
  return new Promise(resolve => setTimeout(resolve, 2000))
}