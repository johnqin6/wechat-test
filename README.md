# 微信公众号开发

## 微信公众号简介  

### 微信公众号是什么？
 
微信公众平台，是给个人、企业和组织提供业务服务与用户管理能力的全新服务平台。简单地说，就是一个提供服务的平台。
[官方网址](https://mp.weixin.qq.com/)

### 微信公众号的分类  
- 订阅号 
  + 简介：为个人和媒体提供一种新的信息传播方式，主要功能是在微信给用户传递资讯。（功能类似报纸杂志，提供新闻或娱乐趣事）
  + 适用人群：个人，媒体
  + 群发次数：订阅号（认证用户，非认证用户）1天内可群发1条消息
- 服务号 （需要企业资格证认证）
  + 简介：为企业和组织提供更强大的业务服务与用户管理能力，主要偏向服务类交互(功能类似银行)
  + 适用人群：企业，政府，其他组织
  + 群发次数：服务号一个月内可发送4条群发消息
- 企业微信（原企业号）
  + 简介：小程序是一种新的开放能力，开发者可以快速开发一个小程序。小程序可以在微信内被便捷地获取和传播，同时具有出色的使用体验。
- 微信小程序
  + 简介：企业微信继承企业号所有能力，同时为企业提供专业的通讯工具，丰富的办公应用与api,助力企业高效沟通与办公。

### 微信公众号注册
请自行前往[官网](https://mp.weixin.qq.com/)选择注册，依次按照官网要求依次填写即可


### 微信公众号订阅号功能
登录成功后，扫描微信二维码，就会进入订阅号后台。功能如下：  
![功能试图](./images/test1.png)  

平台内置功能比较简单（可视化使用，无需代码）可自行了解学习，这里就不介绍了。

微信公众号除内置功能，也可自行开发功能。

个人订阅号(未认证)的接口权限有限，我们可以使用微信平台提供的测试号，测试号的接口权限开启的比较全，
这是微信平台为开发者开发学习提供便利。

测试号账户位置：开发者工具 -> 公众平台测试账户 -> 点击进入会提示登录，选择微信扫描进入 -> 进入后会有appid
和appsecret（这就是我们的账户信息）和其他配置信息    

公众号开发原理
![原理](./images/wechat.png) 

流程解析：  
用户客户端发送消息到微信服务器，微信服务器（根据用户客户端携带的开发者网址）将消息发送给开发者服务器，开发者服务器接收消息，响应内容给微信服务器，微信服务器再转发给用户。此过程微信服务器充当中间转发者.

### 公众号文档

[开发者文档地址](https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html)  


## 公众号开发相关知识点

### 与微信开发服务器通信

#### 验证服务器的有效性
微信公众号开发中需要微信服务器和开发者服务器相互验证，以保证消息的安全可靠。  
步骤如下：   
1. 微信服务器分辨开发者服务器是哪个
   - 测试号管理页面上填写开发者服务器地址
     - 使用ngrok内网穿透（或国内花生壳），将本地端口开启的服务映射外网跨域访问一个网址
     - ngrok http 3000
   - 填写token
     - 参与微信签名加密的一个参数
2. 开发者服务器 - 验证消息是否来自于微信服务器
   目的：计算得出signature微信加密签名，和微信传递过来的signature对比，
        一样说明来自微信服务器。
   1. 将参与微信加密签名的三个参数(timestamp, nonce,token)安装字典顺序排序并组合形成一个数组
   2. 将数组里所有参数拼接成一个字符串，进行sha1加密
   3. 加密完成就生成一个signatrue,和微信发送的进行对比
    一样就说明来自微信服务器，返回echostr给微信服务器,否则返回error  

代码演示：
```javascript
const config = require('./config')
// 接收处理所有消息
app.use((req, res, next) => {
  console.log(req.query) // 微信服务器提交的参数
  /**
   * { signature: '266349d29feadfa1d9a914e9db6e2860eb633ba0', // 微信的加密签名
       echostr: '3286686535251247103', // 微信的随机字符串
       timestamp: '1569031743', // 发送请求的时间戳
       nonce: '393482375' }  // 微信的随机数字
   */
  const {signature, echostr, timestamp, nonce} = req.query
  const {token} = config

  // 1. 将参与微信加密签名的三个参数(timestamp, nonce,token)安装字典顺序排序并组合形成一个数组
  const arr = [timestamp, nonce, token]
  const arrSort = arr.sort()
  // 2. 将数组里所有参数拼接成一个字符串，进行sha1加密
  const str = arr.join('')
  const sha1Str = sha1(str)

  //  3. 加密完成就生成一个signatrue,和微信发送的进行对比
  if (sha1Str === signature) {
    // 一样就说明来自微信服务器，返回echostr给微信服务器
    res.send(echostr)
  } else {
    res.send('error')
  }
})
```

简化代码：  
```javascript
const {signature, echostr, timestamp, nonce} = req.query
const {token} = config
// 将参与微信加密签名的三个参数(timestamp, nonce,token)按照字典顺序排序并组合形成一个数组并加密
const sha1Str = sha1([timestamp, nonce, token].sort().join(''))
```




#### 获取access_token
access_token 是全局唯一接口调用凭证   
特点：
   - 唯一的
   - 有效期为2小时，提前5分钟请求
   - 接口权限 每天2000次
 请求地址：
   https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
 请求方式：
   GET
因为access_token的接口调用有一定的次数限制，因此在开发中我们需要将access_token保存在本地，同时在下次过期之前再次
调用接口获取新的access_token。  

设计思路：
   1. 首次本地没有，发送请求获取access_token, 保存下来（本地文件）
   2. 第二次及以后
      - 先本地读取文件，判断是否过期
        - 过期： 重新请求获取access_token, 保存下拉覆盖之前文件（保证文件是唯一的） 
        - 未过期：直接使用
 思路整理：
   读取本地文件(readAccessToken)
     - 本地有文件
       - 判断是否过期(isValidAccessToken)
         - 过期：重新请求获取access_token(getAccessToken), 保存下拉覆盖之前文件（保证文件是唯一的）(saveAcessToken)
         - 未过期：直接使用
     - 本地没有文件
       - 发送请求获取access_token, 保存下来（本地文件），直接使用。

代码演示：  
```javascript
// 只需要引入request-promise-native依赖 request依赖是该依赖需要的
const rp = require('request-promise-native') // 用于服务端请求（promise形式）
const request = require('request')
const {appID, appsecret} = require('../config')
const { writeFileAsync, readFileAsync } = require('../utils/tool')
class Wechat {
  constructor () {}
  /**
   * 获取access_token
   */
  getAccessToken () {
    const url = `${api.accessToken}&appid=${appID}&secret=${appsecret}`
    // 发送请求 返回promise是为了将请求结果返回出去
    return new Promise((resolve, reject) => {
      rp({
        method: 'GET',
        url,
        json: true // 将数据转为json
      }).then(res => {
        console.log(res)
        /**
         * { access_token:
              '25_DoJkeo7h0vzelTJH_-sgKqddyp0huo6iq-Ur4qdRMYwqTOQm0aBySJ6Fa6mu7YhKPaN2ruGNSpp73RSQbSXA2lPfiyoI8l1JmsPVF7ecEzuEv3qlG-ZaoYa6FLoELVhAGAWDN',
             expires_in: 7200 }
         */
        // 设置access_token的过期时间 300 = 5 * 60
        res.expires_in = Date.now() + (res.expires_in - 300) * 1000
        resolve(res)
      }).catch(err => {
        console.log(err)
        reject('getAccessToken出错：' + err)
      })
    })
  }

  /**
   * 保存access_token
   * @param accessToken 要保存的凭证
   */
  saveAccessToken(accessToken) {
    return writeFileAsync(accessToken, 'accessToken.txt')
  }

  /**
   * 读取本地文件的access_token
   */
  readAccessToken() {
    return readFileAsync('accessToken.txt')
  }

  /**
   * 检测accessToken是否是有效的
   * @param {*} data
   */
  isValidAccessToken(data) {
    // 检测传入的参数是否是有效的
    if (!data && !data.access_token && !data.expires_in) {
      // 代表access_token无效的
      return false
    }
    // 检测access_token是否在有效期内
    /** 
     *  if (data.expires_in < Date.now()) {
     *   // 过期了
     *  return false
     *  } else {
     *   return true
     *  }
     */
    return data.expires_in > Date.now()
  }
  /**
   * 获取没有过期的access_token
   * return {Promise<any>} access_token
   */
  fetchAccessToken () {
    if (this.access_token && this.expires_in && this.isValidAccessToken(this)) {
      // 说明之前保存过access_token, 并且它是有效的，直接使用
      return Promise.resolve({
        access_token: this.access_token,
        expires_in: this.expires_in
      })
    }
    // fetchAccessToken函数的返回值
    return this.readAccessToken().then(async res => {
        // 本地有文件
        // 判断是否过期
        if (this.isValidAccessToken(res)) {
          // 有效的
          // resolve(res) 
          return Promise.resolve(res) //为方便继续.then
        } else { // 过期了
          // 发送请求获取access_token
          const res = await this.getAccessToken()
          // 保存下来，直接使用
          await this.saveAccessToken(res)
          // 将请求回来的access_token返回出去
          // resolve(res)
          return Promise.resolve(res)
        }
      }).catch(async err => {
        // 本地没有文件
        // 发送请求获取access_token
        const res = await this.getAccessToken()
        // 保存下来，直接使用
        await this.saveAccessToken(res)
        // 将请求回来的access_token返回出去
        // resolve(res)
        return Promise.resolve(res)
      }).then(res => {
        // 将access_token挂载到this上
        this.access_token = res.access_token
        this.expires_in = res.expires_in
        // 返回res包装了一层promise对象(此对象成功的状态)
        // this.readAccessToken（）最终的返回值
        return Promise.resolve(res)
      })
  }
}
```

### 微信公众号网页开发

进行微信公众号网页开发需要使用微信JS-SDK，微信JS-SDK是微信公众平台 面向网页开发者提供的基于微信内的网页开发工具包。
通过使用微信JS-SDK，网页开发者可借助微信高效地使用拍照、选图、语音、位置等手机系统的能力，同时可以直接使用微信分享、扫一扫、卡券、支付等微信特有的能力，为微信用户提供更优质的网页体验。

[js-sdk的文档地址](https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#0) 

使用步骤：  
1. 绑定域名
先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
备注：登录后可在“开发者中心”查看对应的接口权限。
2. 引入JS文件 http://res.wx.qq.com/open/js/jweixin-1.4.0.js  
3. 通过config接口注入权限验证配置  
所有需要使用JS-SDK的页面必须先注入配置信息，否则将无法调用（同一个url仅需调用一次，对于变化url的SPA的web app可在每次url变化时进行调用,目前Android微信客户端不支持pushState的H5新特性，所以使用pushState来实现web app的页面会导致签名失败，此问题会在Android6.2中修复）。 
```javascript
wx.config({
  debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
  appId: '', // 必填，公众号的唯一标识
  timestamp: , // 必填，生成签名的时间戳
  nonceStr: '', // 必填，生成签名的随机串
  signature: '',// 必填，签名
  jsApiList: [] // 必填，需要使用的JS接口列表
});
```
4. 通过ready接口处理成功验证
```javascript
wx.ready(function(){
  // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
});
```
另外使用js-sdk还需要生成js-sdk使用的签名  
生成步骤：  
1. 组合参与签名的四个参数：jsapi_ticket(临时票据)，noncestr(随机字符串)，timestamp(时间戳)，url(当前服务器地址)
2. 将其进行字典排序，以'&'拼接在一起
3. 进行sha1加密，最终生成signature  
```javascript
// 获取票据
  const { ticket } = await wechatApi.fetchJsapiTicket() 
  // 随机字符串
  const noncestr = Math.random().toString().split('.')[0]
  // 时间戳
  const timestamp = Date.now()
  
  // 1. 组合参与签名的四个参数
  const arr = [
    `jsapi_ticket=${ticket}`,
    `noncestr=${noncestr}`,
    `timestamp=${timestamp}`,
    `url=${url}/search`
  ]

  // 2. 将其进行字典排序，以'&'拼接在一起
  const str = arr.sort().join('&')

  // 3. 进行sha1加密，最终生成signature
  const signature = sha1(str)
```
其中ticket需要我们调用微信提供的接口获得：https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi   

jsapi_ticket是公众号用于调用微信JS接口的临时票据。正常情况下，jsapi_ticket的有效期为7200秒，通过access_token来获取。由于获取jsapi_ticket的api调用次数非常有限，频繁刷新jsapi_ticket会导致api调用受限，影响自身业务，开发者必须在自己的服务全局缓存jsapi_ticket 。    
jsapi_ticket的获取和access_token的获取差不多，在此就不贴了。可以前往`wechat/wechat.js`查看代码。  

### 使用谷歌开发的爬虫库爬取数据
在项目开发中不免需要一些相关数据，这是就需要爬虫登场了。
在此我介绍puppeteer ---谷歌开发的爬虫库 开发环境使用，线上环境无需使用。

使用步骤：
```javascript
const url = 'https://movie.douban.com/cinema/nowplaying/suzhou/'
// 1. 打开浏览器
const browser = await puppeteer.launch()
// 2. 创建tab标签页
const page = await browser.newPage()
// 3. 跳转到指定网址
await page.goto(url, {
  waitUntil: 'networkidle2' // 等待网络空闲时，再跳转加载页面
})
// 4. 等待网址加载完成，开始抓取数据
let result = await page.evaluate(() => {
  ... //抓取数据的代码操作
})

// 5. 关闭浏览器
await browser.close()
```

### 上传数据到七牛服务器
在微信开发中需要把一些图片，视频等一些媒体保存到第三方服务器（微信服务器只保存资源3天），这是我们就可以把
资源保存到七牛服务器上（七牛服务器的对象存储功能是免费的）
```javascript
const qiniu = require('qiniu') // 引入qiniu（七牛）

const accessKey = 'VEXRGMpVe2MJGzdRYlxKX1Jt3-CqY4fl_Y3SUS'
const secretKey = '_WD9LklsklhQTOgEmPjHe7-cT0Rr545_2GeMX82X'

// 定义鉴权对象
const mac = new qiniu.auth.digest.Mac(accessKey, secretKey);
// 定义配置对象
const config = new qiniu.conf.Config()

// 存储区域
config.zone = qiniu.zone.Zone_z1
// bucketManager对象上就有所有的方法
const bucketManager = new qiniu.rs.BucketManager(mac, config)
var bucket = "students"; // 存储空间名称

module.exports = (resUrl, key) => {
  console.log(resUrl, key)
  /**
   * resUrl 网络资源的地址
   * bucket 存储空间名称
   * key 重命名网络资源的名称
   */
  return new Promise((resolve, reject) => {
    return bucketManager.fetch(resUrl, bucket, key, function(err, respBody, respInfo) {
      console.log(err, respBody, respInfo)
      if (err) {
        console.log(err);
        //throw err;
        reject('上传七牛出了问题：' + err)
      } else {
        if (respInfo.statusCode == 200) {
          console.log('文件上传成功')
          resolve()
        }
      }
    });
  })
}
```

### 扩充知识点 
- 视频播放器插件dplayer 
  + 官网：http://dplayer.js.org/zh/guide.html
  + 介绍：DPlayer 是一个支持弹幕的 HTML5 视频播放器。支持 Bilibili 视频和 danmaku，实时视频（HTTP Live Streaming，M3U8格式）以及 FLV 格式。
