const mongoose = require('mongoose')

// 获取Schema
const Schema = mongoose.Schema

const danmusSchema = new Schema({
  danbanId: String,
  author: String,
  time: Number,
  text: String,
  color: String,
  type: Number
})

// 创建模型对象
const Danmus = mongoose.model('Danmus', danmusSchema)

module.exports = Danmus
