const mongoose = require('mongoose')

// 获取Schema
const Schema = mongoose.Schema

const theaterSchema = new Schema({
  title: String,
  rating: Number,
  runtime: String,
  directors: String,
  casts: String,
  image: String,
  doubanId: {
    type: Number,
    unique: true // 唯一值
  },
  genre: [String],
  summary: String,
  releaseDate: String,
  posterKey: String, // 图片上传到七牛中，返回的key值
  createTime: {
    type: Date,
    default: Date.now()
  }
})

// 创建模型对象
const Theaters = mongoose.model('Theaters', theaterSchema)

module.exports = Theaters
