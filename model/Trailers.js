const mongoose = require('mongoose')

// 获取Schema
const Schema = mongoose.Schema

const trailersSchema = new Schema({
  title: String,
  rating: Number,
  runtime: String,
  directors: String,
  casts: [String],
  image: String,
  doubanId: {
    type: Number,
    unique: true // 唯一值
  },
  genre: [String],
  summary: String,
  releaseDate: String,
  link: String,
  posterKey: String, // 图片上传到七牛中，返回的key值
  coverKey: String, // 视频的封面图
  videoKey: String, // 视频
  createTime: {
    type: Date,
    default: Date.now()
  }
})

// 创建模型对象
const Trailers = mongoose.model('Trailers', trailersSchema)

module.exports = Trailers
